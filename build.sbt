name := "emacs-sbt"

version := "0.1"

scalaVersion := "2.11.11"

ensimeIgnoreScalaMismatch in  ThisBuild := true

libraryDependencies +=
  "com.typesafe.akka" %% "akka-actor" % "2.4.1"
