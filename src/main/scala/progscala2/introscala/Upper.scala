package progscala2.introscala

object UpperHelpers {
  def upper(strings: String*): Seq[String] = strings.map(_.toUpperCase())
}

object Upper {
  def main(args: Array[String]): Unit = {
    println{
      UpperHelpers.upper("Hello", "World")
    }
  }
}
