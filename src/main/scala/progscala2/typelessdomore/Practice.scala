object TypeLessDoMorePractice extends App {

  /*
   * Immutable Array, i.e, you can change the reference of
   * array but you can change the elements in it
   */
  val array: Array[String] = new Array(5)
  println {
    array
  }

  array(0) = "Hello"
  array(1) = "World"

  array foreach println

  var stockprice: Double = 100.0
  stockprice = 200.0
  println(stockprice)

  /*
   *  vals and vars
   */

  class Person(val name: String, var age: Int)
  val p = new Person("Gnanesh Kunal", 19)
  println(p.name)
  println(p.age)
  p.age = 20 // cause i'm now 20 :P
  println(p.age)

  /*
   *  Ranges
   */
  println(1 to 10)
  println(1 until 10)
  println(1 to 10 by 3)
  println(10 to 1 by -3)
  println(1L to 10L by 3)
  println(1.1f to 10.3f by 3.1f)
  println(1.1f to 10.3f by 0.5f)
  println(1.1 to 10.3 by 3.1)
  println('a' to 'g' by 3)
  println(BigInt(1) to BigInt(10) by 3)
  println(BigDecimal(1.1) to BigDecimal(10.3) by 3.1)

  /*
   *  Partial Functions
   */

  val pf1: PartialFunction[Any, String] = { case s: String => "YES" }
  val pf2: PartialFunction[Any, String] = { case d: Double => "YES" }

  val pf = pf1 orElse pf2

  def tryPF(x: Any, f: PartialFunction[Any, String]): String =
    try { f(x).toString } catch { case _: MatchError => "ERROR!" }
  def d(x: Any, f: PartialFunction[Any, String]) =
    f.isDefinedAt(x).toString

  println("|  pf1 - String | pf2 - Double | pf - All")
  println("|x | def? | pf1(x) | def? | pf2(x) | def? | pf(3)")

  List("str", 3.14, 10) foreach {x =>
    println(x.toString, d(x, pf1), tryPF(x, pf1), d(x, pf2), tryPF(x, pf2), d(x, pf), tryPF(x, pf))
  }

  /*
   * Method Declarations
   */

  case class Point(x: Double = 0.0, y: Double = 0.0) {

    def shift(deltax: Double = 0.0, deltay: Double = 0.0) =
      copy(x + deltax, y + deltay) // copy method created automatically for case classes
  }

  val p1 = new Point(x = 3.3, y = 4.4)
  println(p1)

  val p2 = p1.copy(y = 6.6)
  println(p2)
  val p3 = p1.shift(2.0, 3.0)
  println(p3)
  println(p1)


  abstract class Shape() {

    def draw(offset: Point = Point(0.0, 0.0))(f: String => Unit): Unit =
      f(s"draw(offset = $offset), ${this.toString}")
  }

  case class Circle(center: Point, radius: Double) extends Shape

  case class Rectangle(lowerLeft: Point, height: Double, width: Double) extends Shape

  /*
   * CONCURRENT FUTURE
   */
  import scala.concurrent.Future
  import scala.concurrent.ExecutionContext.Implicits.global

  def sleep(millis: Long) = {
    Thread.sleep(millis)
  }
  // Busy work
  def doWork(index: Int) = {
    sleep((math.random * 1000).toLong)
    index
  }

  println("Started")

  (1 to 5) foreach { index =>
    val future = Future {
      doWork(index)
    }

    future onSuccess {
      case answer: Int => println(s"Success! returned: $answer")
    }
    future onFailure {
      case th: Throwable => println(s"FAILURE! returned: $th")
    }
  }

  sleep(1000)
  println("Finito!")

  /*
   * Nesting Method Definitions and Recursion
   */

  def factorial(i: Int): Long = {
    @annotation.tailrec def fact(i: Int, accumulator: Int): Long = {
      if (i <= 1) accumulator
      else fact(i -1, i * accumulator)
    }
    fact(i, 1)
  }

  (0 to 5) foreach (i => print(" " + factorial(i)))

  /*
   *  Inferring Type Information
   */
  import scala.collection.immutable.HashMap
  val intToStringMap: HashMap[Integer, String] = new HashMap
  val intToStringMap2 = new HashMap[Integer, String]

  object StringUtilV1 {
    def joiner(strings: String*): String = strings.mkString("-")

    // Here the return type is required, as the type inference doesn't happen
    def joiner(strings: List[String]): String = joiner(strings: _*)
  }

  println {StringUtilV1.joiner("Hello", "World")}
  println {StringUtilV1.joiner(List("Hello", "World2"))}

  // Right way to do about return types
  def makeList(strings: String*) = {
    if (strings.length == 0)
      Nil // or List.Empty[String]
    else
      strings.toList
  }
  println (makeList("23", "ASDDSA"))

  val list: List[String] = makeList()

  /*
   * STRING LITERALS
   */

  def hello(name: String) = s"""Welcome!
Hello, $name!
|We're glad to meet you.
|        This is something.""".stripMargin

  println{hello("Gnanesh")}

  /*
   * TUPLE LITERALS
   */

  val t1: (Int, String) = (1, "two")
  val t2: Tuple2[Int, String] = (1, "two")

  val t = ("Hello", 1, 2.3)
  println(t)
  println(t._1)
  val(t3, t4, t5) = ("World", '!', 0x22)
  println(t3 + ", " + t4 + ", " + t5)

  val stateCapitals = Map(
    "Alabama" -> "Montgomery",
    "Alaska" -> "Juneau"
  )
  println("Get the capitals: ")
  println("Alabama: " + stateCapitals.get("Alabama"))
  println("Unknown: " + stateCapitals.get("Unknown"))

  println("Get the capitals themselves out of the Options: ")
  println("Alabama: " + stateCapitals.get("Alabama").get)
  println("Unknown: " + stateCapitals.get("Unknown").getOrElse("Oops!"))

  /*
   * PACKAGES
   */
  /*
  package com {
    package example {
      package pkg1 {
        class Class11 {
          def m = "m11"
        }
        class Class12 {
          def m = "m12"
        }
      }
      package pkg2 {
        class Class21 {
          def m = "m21"
          def makeClass11 = {
            new pkg1.Class11
          }
        }
      }
      package pkg3.pkg31.pkg311 {
        class Class311 {
          def m = "m21"
        }
      }
    }
  }
   */





}


