object Implicits extends App {

  /*
   * Implicit Arguments
   */

  def calcTax(amount: Float)(implicit rate: Float): Float = amount * rate

  {
    implicit val currentTaxRate = 0.08F
    val tax = calcTax(50000F)
    println(tax)
  }

  object SimpleStateSalesTax {
    implicit val rate: Float = 0.05F
  }

  case class ComplicatedSalesTaxData(
    baseRate: Float,
    isTaxHoliday: Boolean,
    storeId: Int
  )

  object ComplicatedSalesTax {
    private def extraTaxRateForStore(id: Int): Float  = {
      0.0F
    }

    implicit def rate(implicit cstd: ComplicatedSalesTaxData): Float =
      if (cstd.isTaxHoliday) 0.0F
      else cstd.baseRate + extraTaxRateForStore(cstd.storeId)
  }
  {
    import SimpleStateSalesTax.rate

    val amount = 100F
    println(s"Tax on amount = ${calcTax(amount)}")
  }
  {
    import ComplicatedSalesTax.rate
    implicit val myStore = ComplicatedSalesTaxData(0.06F, false, 1010)

    val amount = 100F
    println(s"Tax on $amount = ${calcTax(amount)}")
  }

  /*
   * Using Implicitly
   */
  import math.Ordering

  case class MyList[A](list: List[A]) {
    def sortBy1[B](f: A => B)(implicit ord: Ordering[B]): List[A] =
      list.sortBy(f)(ord)

    def sortBy2[B: Ordering](f: A => B): List[A] =
      list.sortBy(f)(implicitly[Ordering[B]])
  }

  val list = MyList(List(1, 3, 5, 2, 4))

  println(list sortBy1 (i => -i))
  println(list sortBy2 (i => -i))

  val l1 = List(1, 2, 3)
  println(l1)
  //println(l1.toMap)
  val l2 = List("one" -> 1, "two" -> 2, "three" -> 3)
  println(l2)
  println(l2.toMap)

  /*
   *  JVM forgets types
   */

  /*
   object C {
    def m(seq: Seq[Int]): Unit = println(s"Seq[Int]: $seq")
    def m(seq: Seq[String]): Unit = println(s"Seq[String]: $seq")
  }
   */

  // with types
  object M {
    implicit object IntMarker
    implicit object StringMarker

    def m(seq: Seq[Int])(implicit i: IntMarker.type): Unit = println(s"Seq[Int]: $seq")
    def m(seq: Seq[String])(implicit s: StringMarker.type): Unit = println(s"Seq[String]: $seq")
  }

  import M._
  m(List(1, 2, 3))
  m(List("one", "two", "three"))

  /*
   * Type Class Pattern
   */

  case class Address(street: String, city: String)
  case class Person(name: String, address: Address)

  trait ToJSON {
    def toJSON(level: Int = 0): String

    val IDENTATION = "  "
    def indentation(level: Int = 0): (String, String) =
      (IDENTATION * level, IDENTATION * (level + 1))
  }

  implicit class AddressToJSON(address: Address) extends ToJSON {
    def toJSON(level: Int = 0): String = {
      val (outdent, indent) = indentation(level)
      s"""{
|${indent}"street": "${address.street}",
|${indent}"city": "${address.city}"
|$outdent
}""".stripMargin
    }
  }

  implicit class PersonToJSON(person: Person) extends ToJSON {
    def toJSON(level: Int = 0): String = {
      val (outdent, indent) = indentation(level)
      s"""{
|${indent}"name": "${person.name}",
|${indent}"address": ${person.address.toJSON(level + 1)}
|$outdent}""".stripMargin
}
    }

  val a = Address("1 Scala Lane", "Anytowm")
  val p = Person("Buck Trends", a)

  println(a.toJSON())
  println()
  println(p.toJSON())


}
