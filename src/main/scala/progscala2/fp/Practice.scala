object FunctionalProgramming extends App {

  def factorial(i: Int): Long = {
    def fact(i: Int, accumulator: Int): Long = {
      if (i <= 1) accumulator
      else fact(i - 1, i * accumulator)
    }
    fact(i, 1)
  }

  (0 to 5).foreach(i => print(" " + factorial(i)))
  println("")

  println((1 to 10) filter (_ % 2 == 0) map (_ * 2) reduce (_ * _))

  /*
   * Anonymous Functions, Lambdas, and Closures
   */
  var factor = 2
  val multiplier = (i: Int) => i * factor

  println((1 to 10) filter (_ % 2 == 0) map multiplier reduce (_ * _))

  factor = 3
  println((1 to 10) filter (_ % 2 == 0) map multiplier reduce (_ * _))

  def m1 (multiplier: Int => Int) = {
    (1 to 10) filter (_ % 2 == 0) map multiplier reduce (_ * _)
  }

  def m2: Int => Int = {
    val factor = 2
    val multiplier = (i: Int) => i * factor
    multiplier
  }

  println(m1(m2))

  /*
   * Methods as Functions
   */

  object Multiplier {
    var factor = 2

    def multiplier(i: Int) = i * factor
  }

  println((1 to 10) filter (_ % 2 == 0) map Multiplier.multiplier reduce (_ * _))

  Multiplier.factor = 3

  println((1 to 10) filter (_ % 2 == 0) map Multiplier.multiplier reduce (_ * _))

  def factorial(i: BigInt): BigInt =
    if (i == 1) i
    else i * factorial(i - 1)

  for (i <- 1 to 10)
    println(s"$i:\t${factorial(i)}")

  /*
   * Tail Calls and Tail-Call Optimization
   */

  def factorial2(i: BigInt): BigInt = {
    @annotation.tailrec
    def fact(i: BigInt, accumulator: BigInt): BigInt =
      if (i == 1) accumulator
      else fact(i - 1, i * accumulator)

    fact(i, 1)
  }
  for (i <- 1 to 10)
    println(s"$i:\t${factorial2(i)}")

  /*
   * Trampoline for Tail Calls
   */
  import scala.util.control.TailCalls._

  def isEven(xs: List[Int]): TailRec[Boolean] =
    if (xs.isEmpty) done(true) else tailcall(isOdd(xs.tail))

  def isOdd(xs: List[Int]): TailRec[Boolean] =
    if (xs.isEmpty) done(false) else tailcall(isEven(xs.tail))

  for (i <- 1 to 5) {
    val even = isEven((1 to i).toList).result
    println(s"$i is even? $even")
  }
  /*
   * Partially Applied Functions Versus Partial Functions
   */

  def cat1(s1: String)(s2: String) = s1 + s2
  val hello = cat1("Hello") _
  println(hello("World"))

  val inverse: PartialFunction[Double, Double] = {
    case d if d != 0.0 => 1.0 / d
  }
  println(inverse(1.0))
  println(inverse(2.0))

  /*
   * Curring and Other Transformations on Functions
   */
  def cat2(s1: String) = (s2: String) => s1 + s2
  val cat2hello = cat2("Hello")
  println(cat2hello("World"))

  def cat3(s1: String, s2: String) = s1 + s2
  println(cat3("hello", "world"))

  val cat3Curried = (cat3 _).curried
  println(cat3Curried("Hello")("World"))

  val cat3Uncurried = Function.uncurried(cat3Curried)
  println(cat3Uncurried("Hello", "World"))

  def mult(d1: Double, d2: Double, d3: Double) = d1 * d2 * d3

  val d3 = (2.2, 3.3, 4.4)
  println(mult(d3._1, d3._2, d3._3))

  val multTupled = Function.tupled(mult _)
  println(multTupled(d3))

  /*
   * Functional Data Structures
   */
  val list1 = List("Programming", "Scala")
  val list2 = "People" :: "should" :: "read" :: list1

  println(list2)
  println("Programming" :: "Scala" :: Nil)
  println(list1 ++ list2)

  val seq1 = Seq("Programming", "Scala")
  val seq2 = "People" +: "should" +: "read" +: seq1
  val seq3 = "Programming" +: "Scala" +: Seq.empty
  val seq4 = seq1 ++ seq2

  // list :+ x and x +: list
  println(seq4 :+ "THis")

  val vect1 = Vector("Programming", "Scala")
  println(vect1)

  val stateCapitals = Map(
    "Alabama" -> "montgomery",
    "Alaska" -> "Juneau")

  val lengths = stateCapitals map {
    kv => (kv._1, kv._2.length)
  }
  println(lengths)

  val stateCapitals2 = stateCapitals + (
    "Virginia" -> "Richmond"
  )

  val states = Set("Alabama", "Alaska", "Wyoming")
  val lengths2 = states map (st => st.length)
  println(lengths2)

  /*
   * Traversal
   */

  List(1, 2, 3, 4, 5) foreach {i => println("Int: " + i)}
  stateCapitals foreach {case (k, v) => println(k + ": " + v)}

  val l = List(1, 2, 3, 4).fold (3) (_ * _)
  println(l)
}
