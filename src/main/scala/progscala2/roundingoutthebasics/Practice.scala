object RoundingOutTheBasics extends App {

  //back-quotes
  def `test that addition works` = assert(1 + 1 == 2)

  //optional parenthesis
  def isEven(i: Int) = (i % 2) == 0
  List(1, 2, 3, 4) filter isEven foreach println

  // for
  val dogBreeds = List("Doberman", "Yorkshire Terrier", "Dachshund", "Scottish Terrier", "Great Dane", "Portuguese Water Dog")

  for (breed <- dogBreeds)
    println(breed)

  //guards

  for (breed <- dogBreeds
       if breed.contains("Terrier")
  ) println(breed)

  for (breed <- dogBreeds
       if breed.contains("Terrier")
       if !breed.startsWith("Yokshire")
  ) println(breed)

  // YIELD
  val filteredBreeds = for {
    breed <- dogBreeds
    if breed.contains("Terrier") && !breed.startsWith("Yorkshire")
  } yield breed

  println(filteredBreeds)

  // EXPANDED SCOPE
  for {
    breed <- dogBreeds
    upcasedBreed = breed.toUpperCase()
  } println(upcasedBreed)

/*
  val dogBreeds2 = List(Some("Doberman"), None, Some("Yorkshire Terrier"),
                        Some("Dachshund"), None, Some("Scottish Terrier"),
                        None, Some("Great Dabe"), Some("Portuguese Water Dog"))
  println("first pass: ")
  for {
    breedOption <- dogBreeds2
    breed <- breedOption
    upcasedBreed = breed.toUpperCase()
  } println(upcasedBreed)
 */

  // TRY CATCH THROW
  import scala.io.Source
  import scala.util.control.NonFatal

  /*
  def countLines(fileName: String) = {
    println()
    var source: Option[Source] = None

    try {
      source = Some(Source.fromFile(fileName))
      val size = source.get.getLines.size
      println(s"file $fileName has $size lines")
    } catch {
      case NonFatal(ex) => println(s"Non fatal exception! $ex")
    } finally {
      for (s <- source) {
        println(s"Closing $fileName...")
        s.close
      }
    }

  }
   */

  // ENUMS
  object Breed extends Enumeration {
    type Breed = Value
    val doberman = Value("Doberman Pinscher")
    val yorkie = Value("Yorkshire Terrier")
    val scottie = Value("Scottish Terrier")
    val dane = Value("Great Dane")
    val portie = Value("Portuguese Water Dog")
  }
  import Breed._

  println{"ID\tBreed"}
  for (breed <- Breed.values) println(s"${breed.id}\tbreed")
  println("\nJust Terriers:")
  Breed.values filter (_.toString.endsWith("Terrier")) foreach println

  // TRAITS
  class ServiceImportante(name: String) {
    def work(i: Int): Int = {
      println(s"ServiceImportante: Doing important work! $i")
      i + 1
    }
  }

  val service1 = new ServiceImportante("uno")

  (1 to 3) foreach (i => println(s"Result: ${service1.work(i)}"))

  trait Logging {
    def info (message: String): Unit
    def warning (message: String): Unit
    def error (message: String): Unit
  }

  trait StdoutLogging extends Logging {
    def info (message: String): Unit = println(s"INFO: $message")
    def warning (message: String): Unit = println(s"WARNING: $message")
    def error (message: String): Unit = println(s"ERROR: $message")
  }

  val service2 = new ServiceImportante("dos") with StdoutLogging {
    override def work(i: Int): Int = {
      info(s"Starting work: i = $i")
      val result = super.work(i)
      info(s"Ending work: i = $i, result = $result")
      result
    }
  }
    (1 to 3) foreach (i => println(s"Result: ${service2.work(i)}"))

  // multiple instances
  class LoggedServiceImportante(name: String)
      extends ServiceImportante(name) with StdoutLogging


}
