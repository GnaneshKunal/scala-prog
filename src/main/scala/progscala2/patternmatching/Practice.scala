object PatternMatching extends App {
  val bools = Seq(true, false)

  for (bool <- bools) {
    bool match {
      case true => println("Got heads")
      case false => println("Got tails")
    }
  }

  // Simple alternative
  for (bool <- bools) {
    val which = if (bool) "heads" else "tails"
    println("Got " + which)
  }
  /*
   * Values, Variables, and Types in Matches
   */
  for {
    x <- Seq(1, 2, 2.7, "one", "two", 'four)
  } {
    val str = x match {
      case 1 => "int 1"
      case 2 => "other int: 2"
      case 2.7 => "a double: 2.7"
      case "one" => "string one"
      case s: String => "other string: " + s
      case unexpected => "unexpected value: " + unexpected
    }
    println (str)
  }

  // slight variation of the previous example
  for {
    x <- Seq(1, 2, 2.7, "one", "two", 'four)
  } {
    val str = x match {
      case 1 => "int 1"
      case _: Int => "other int: " + x
      case _: Double => "a double: " + x
      case "one" => "string one"
      case _: String => "other string: " + x
      case _ => "unexpected value: " + x
    }
    println(str)
  }

  def checkY(y: Int) = {
    for {
      x <- Seq(99, 100, 101)
    } {
      val str = x match {
        case `y` => "found y!" // use `backticks` to match aganist your args
        case i: Int => "int: " + i
      }
      println(str)
    }
  }

  checkY(101)

  // Several differnt matches using one case
  for {
    x <- Seq(1, 2, 2.7, "one", "two", 'four)
  } {
    val str = x match {
      case _: Int | _: Double => "a number: " + x
      case "one" => "string one"
      case _: String => "other string: " + x
      case _ => "unexpected value: " + x
    }
    println(str)
  }

  /*
   * Matching on Sequences
   */
  val nonEmptySeq = Seq(1, 2, 3, 4, 5)
  val emptySeq = Seq.empty[Int]
  val nonEmptyList = List(1, 2, 3, 4, 5)
  val emptyList = Nil
  val nonEmptyVector = Vector(1, 2, 3, 4, 5)
  val emptyVector = Vector.empty[Int]
  val nonEmptyMap = Map("one" -> 1, "two" -> 2, "three" -> 3)
  val emptyMap = Map.empty[String, Int]

  def seqToString[T](seq: Seq[T]): String = seq match {
    case head +: tail => s"$head +: " + seqToString(tail)
    case Nil => "Nil"
  }

  for (seq <- Seq(
         nonEmptySeq, emptySeq, nonEmptyList, emptyList,
         nonEmptyVector, emptyVector, nonEmptyMap.toSeq, emptyMap.toSeq
       )) {
    println(seqToString(seq))
  }

  val s1 = (1 +: (2 +: (3 +: (Nil))))
  println(s1)

  val s2 = (("one", 1) +: (("two", 2) +: (("three", 3) +: Nil)))
  println(s2)

  // Convert List -> Map
  val m = Map(s2 :_*)
  println(m)

  /*
   * Matching on Tuples
   */

  val langs = Seq(
    ("Scala", "Martin", "Odersky"),
    ("Clojure", "Rich", "Hickey"),
    ("Lisp", "John", "McCarthy")
  )

  for (tuple <- langs) {
    tuple match {
      case ("Scala", _, _) => println("Found Scala")
      case (lang, first, last) =>
        println(s"Found other language: $lang ($first, $last)")
    }
  }

  /*
   * Guards in case Clauses
   */
  for (i <- Seq(1, 2, 3, 4)) {
    i match {
      case _ if i % 2 == 0 => println(s"even: $i")
      case _ => println(s"odd: $i")
    }
  }

  /*
   * Matching on case Clauses
   */
  case class Address(street: String, city: String, country: String)
  case class Person(name: String, age: Int, address: Address)

  val alice = Person("Alice", 25, Address("1 Scala Lane", "Chicago", "USA"))
  val bob = Person("Bob", 29, Address("2 Java Ave.", "Miami", "USA"))
  val charlie = Person("Charlie", 32, Address("3 Python Ct.", "Boston", "USA"))

  for (person <- Seq(alice, bob, charlie)) {
    person match {
      case Person("Alice", 25, Address(_, "Chicago", _)) => println("Hi Alice")
      case Person("Bob", 29, Address("2 Java Ave.", "Miami", "USA")) => println("Hi bob")
      case Person(name, age, _) => println(s"Who are you, $age yead-old person named $name?")
    }
  }

  val itemsCosts = Seq(("Pencil", 0.52), ("Paper", 1.35), ("Notebook", 2.43))
  val itemsCostsIndices = itemsCosts.zipWithIndex
  for (itemCostIndex <- itemsCostsIndices) {
    itemCostIndex match {
      case ((item, cost), index) => println(s"$index: $item costs $cost each")
    }
  }

  // Reverse Seq to String
  def reverseSeqToString[T](l: Seq[T]): String = l match {
    case prefix :+ end => reverseSeqToString(prefix) + s" :+ $end"
    case Nil => "Nil"
  }

  for (seq <- Seq(nonEmptyList, nonEmptyVector, nonEmptyMap.toSeq)) {
    println(reverseSeqToString(seq))
  }

  def windows[T](seq: Seq[T]): String = seq match {
    case Seq(head1, head2, _*) =>
      s"($head1, $head2), " + windows(seq.tail)
    case Seq(head, _*) =>
      s"($head, _) " + windows(seq.tail)
    case Nil => "Nil"
  }

  for (seq <- Seq(nonEmptyList, emptyList, nonEmptyMap.toSeq)) {
    println(windows(seq))
  }

  // also
  def windows2[T](seq: Seq[T]): String = seq match {
    case head1 +: head2 +: tail => s"($head1, $head2), " + windows2(tail)
    case head +: tail => s"($head, _), " + windows2(tail)
    case Nil => "Nil"
  }

  for (seq <- Seq(nonEmptyList, emptyList, nonEmptyMap.toSeq)) {
    println(windows2(seq))
  }

  val seq = Seq(1, 2, 3, 4, 5)
  println(seq)
  val slide2 = seq.sliding(2)
  println(slide2.toSeq)
  println(slide2.toList)
  println(seq.sliding(3, 2).toList)

  /*
   * Matching on Variable Arguments Lists
   */

  object Op extends Enumeration {
    type Op = Value

    val EQ = Value("=")
    val NE = Value("!=")
    val LTGT = Value("<>")
    val LT = Value("<")
    val LE = Value("<=")
    val GT = Value(">")
    val GE = Value(">=")
  }
  import Op._

  // Represent a SQL "WHERE x op value" clause, +op+ is a
  // comparision operator: =, != ...
  case class WhereOp[T](columnName: String, op: Op, value: T)

  // Represent a SQL "WHERE x IN (a, b, ...)" clause
  case class WhereIn[T](columnName: String, val1: T, vals: T*)

  val wheres = Seq(
    WhereIn("state", "IL", "CA", "VA"),
    WhereIn("state", EQ, "IL"),
    WhereOp("name", EQ, "Buck Trends"),
    WhereOp("age", GT, 20)
  )

  for (where <- wheres) {
    where match {
      case WhereIn(col, val1, vals @ _*) =>
        val valStr = (val1 +: vals).mkString(", ")
        println(s"WHERE $col IN ($valStr)")
      case WhereOp(col, op, value) => println(s"WHERE $col $op $value")
      case _ => println(s"ERROR: unknown expression: $where")
    }
  }

  /*
   * Matching on Regular Expressions
   */
  val BookExtractorRE = """Book: title=([^,]+),\s+author=(.+)""".r
  val MagazineExtractorRE = """Magazine: title=([^,]+),\s+issue=(.+)""".r

  val catalog = Seq(
    "Book: title=Programming Scala Second Edition, author=DeanWampler",
    "Magazine: title=The New Yorker, issue=January 2014",
    "Unknown: text=Who put this here??"
  )

  for (item <- catalog) {
    item match {
      case BookExtractorRE(title, author) =>
        println(s"""Book "$title", written by $author""")
      case MagazineExtractorRE(title, issue) =>
        println(s"""Magazine "$title", issue $issue""")
      case entry => println(s"Unrecognized entry: $entry")
    }
  }

  /*
   * More on Binding Variables in case Clauses
   */
  // take the person and address clauses
  for (person <- Seq(alice, bob, charlie)) {
    person match {
      case p @ Person("Alice", 25, address) => println(s"Hi Alice! $p")
      case p @ Person("Bob", 29, a @ Address(street, city, country)) =>
        println(s"Hi ${p.name}! age ${p.age}, in ${a.city}")
      case p @ Person(name, age, _) =>
        println(s"Who are you, $age year-old person named $name? $p")
    }
  }

  /*
   * Sealed Hierarchies and Exhaustive Matches
   */
  sealed abstract class HttpMethod() {
    def body: String
    def bodyLength = body.length
  }

  case class Connect(body: String) extends HttpMethod
  case class Delete(body: String) extends HttpMethod
  case class Get(body: String) extends HttpMethod
  case class Head(body: String) extends HttpMethod
  case class Options(body: String) extends HttpMethod
  case class Post(body: String) extends HttpMethod
  case class Put(body: String) extends HttpMethod
  case class Trace(body: String) extends HttpMethod

  def handle (method: HttpMethod) = method match {
    case Connect (body) => s"connect: (length: ${method.bodyLength}) $body" 
    case Delete (body) => s"delete: (length: ${method.bodyLength}) $body"
    case Get (body) => s"get: (length: ${method.bodyLength}) $body"
    case Head (body) => s"head: (length: ${method.bodyLength}) $body"
    case Options (body) => s"options: (length: ${method.bodyLength}) $body"
    case Post (body) => s"post: (length: ${method.bodyLength}) $body"
    case Put (body) => s"put: (length: ${method.bodyLength}) $body"
    case Trace (body) => s"trace: (length: ${method.bodyLength}) $body"
  }

  val methods = Seq(
    Connect("connect body..."),
    Delete("delete body..."),
    Get("get body..."),
    Head("head body..."),
    Options("options body..."),
    Post("post body..."),
    Put("put body..."),
    Trace("trace body...")
  )

  methods foreach (method => println(handle(method)))

  //also
  val Person(name, age, Address(_, state, _)) =
    Person("Dean", 29, Address("1 Scala Way", "CA", "USA"))

  println(s"$name $age $state")
}
