object ForComprehensionInDepth extends App {

  /*
   * The Elements of for Comprehensions
   */
  object RemoveBlanks {
    def apply(path: String, compressWhiteSpace: Boolean = false): Seq[String] =
      for {
        line <- scala.io.Source.fromFile(path).getLines.toSeq
        if line.matches("""^\s*$""") == false
        line2 = if (compressWhiteSpace) line replaceAll ("\\s+", " ") else line
      } yield line2
  }

  for {
    path2 <- args
    (compress, path) = if (path2 startsWith "-") (true, path2.substring(1)) else (false, path2)
    line <- RemoveBlanks.apply(path, compress)
  } println(line)

  val states = List("Alabama", "Alaska", "Virginia", "Wyoming")
  for {
    s <- states
  } println(s)

  states foreach println

  println{
    for {
      s <- states
    } yield s.toUpperCase
  }

  println(states map (_.toUpperCase))

  println {
    for {
      s <- states
      c <- s
    } yield s"$c-${c.toUpper}"
  }

  println {
    states flatMap (_.toSeq map (c => s"$c-${c.toUpper}"))
  }

  val z @ (x, y) = (1 -> 2)
  println(z)

  val map = Map("one" -> 1, "two" -> 2)

  val list1 = for {
    (key, value) <- map
    i10 = value + 10
  } yield (i10)

  println(list1)
  
}
