class Upper {
  def upper(strings: String*): Seq[String] = {
    strings.map((s: String) => s.toUpperCase())
  }
}

object Upper2 {
  def upper(strings: String*) = strings.map(_.toUpperCase())
}

object HelloWorld extends App {
  val up = new Upper

  println {
    up.upper("Hello", "World")
  }

  val s = "Hello, World"
  println {
    s
  }
  println(1 + 2)
  println {
    s.contains("el")
  }

  println {
    Upper2.upper("Hello", "World")
  }
  args.foreach(printf("%s", _))
}
